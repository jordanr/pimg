# pimg
Pimg is a simple program inspired by projects like dzen2. This program will read image filenames from stdin and display them in a window. This can be used to create scripts that continually read from a source. Please see the `scripts/` folder for examples and inspiration.
