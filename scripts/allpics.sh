#!/bin/bash
#
#  pimg 
#  Copyright (C) 2016  Jordan Russell <jordan.likes.curry@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


dir=$1

if [[ -z $dir ]]; then
    echo "USAGE $0 directory";
    exit 1;
fi


( for file in $dir/*; do
        if ! [[ -f $file && -r $file ]]; then
            continue
        fi
        
        echo "$file"
        sleep 1;
        done ) | pimg
