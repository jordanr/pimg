#!/bin/bash
#
#  pimg 
#  Copyright (C) 2016  Jordan Russell <jordan.likes.curry@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


set -euo pipefail
IFS=$'\n\t'

mpd_music_dir="$HOME/music"
default_current_album_art=""
current_album_art="$default_current_album_art"

if ! [[ -d $mpd_music_dir ]]; then
    echo "You need to adjust your mpd_music_dir in the script";
    exit 1;
fi

function get_album_art () {
    song=""
    if [ $1 == 1 ]; then
        song=$(mpc -f "%file%" current --wait)
    else
        song=$(mpc -f "%file%" current)
    fi
    if [[ "$song" == "" ]]; then
        current_album_art="$default_current_album_art"
    else
        cover=$(echo "$song" | sed 's/\(^.*\)\/.*$/\1\/cover\.jpg/')
        current_album_art="$mpd_music_dir/$cover"

        if ! [[ -f $current_album_art ]]; then
            current_album_art="$default_current_album_art"
        fi
    fi
}

get_album_art 0

{ while true; do
        echo "$current_album_art"
        get_album_art 1
  done } | pimg



