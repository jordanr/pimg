#!/bin/bash

{ for file in $@; do
    echo "$file"
    bname=$(basename "$file")
    dirname=$(dirname "$file")
    newname=$(zenity --entry --text "newname for $file" --entry-text="$bname")
    if [ "$newname" != "$bname" ]; then
        echo "$file" "${dirname}/${newname}" 1>&2
        mv "$file" "${dirname}/${newname}" 1>&2
    fi
done; } | pimg
