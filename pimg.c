/*    pimg - display images from filenames read at command line
 *    Copyright (C) 2016  Jordan Russell <jordan.likes.curry@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glib-object.h>

#define MIN_IMG_SIZE 50

GtkWidget *WIN;
GtkWidget *IMG;
GdkPixbuf *ORIG_BUF;
int ORIG_W;
int ORIG_H;

inline double min(double a, double b)
{
    return a < b ? a : b;
}

void get_new_w_h(double src_w, double src_h,
                 double max_w, double max_h,
                 int *new_w, int *new_h)
{
    double ratio = min(max_w / src_w, max_h / src_h);
    *new_w = (int) (ratio * src_w);
    *new_h = (int) (ratio * src_h);
}

/** shrink image to fit window
 *  do not image size up
 */
void resize_image(GtkContainer *container, gpointer notused)
{
    if (ORIG_BUF == NULL)
        return;
    
    int win_w;
    int win_h;
    gtk_window_get_size(GTK_WINDOW(WIN), &win_w, &win_h);

    if (win_w >= ORIG_W && win_h >= ORIG_H)
        return;
   
    int new_w;
    int new_h;

    get_new_w_h((double) ORIG_W, (double) ORIG_H,
                (double) win_w, (double) win_h,
                &new_w, &new_h);

    if (new_w < MIN_IMG_SIZE || new_h < MIN_IMG_SIZE) // wow that's tiny
        return;

    GdkPixbuf *newbuf = gdk_pixbuf_scale_simple(ORIG_BUF, new_w, new_h,
                                                GDK_INTERP_BILINEAR);

    gtk_image_set_from_pixbuf((GtkImage *) IMG, newbuf);

    g_object_unref((GObject *) newbuf);
}

void set_scaled_image_from_file(const char *fn)
{
    GError *err = NULL;

    int win_w;
    int win_h;
    gtk_window_get_size(GTK_WINDOW(WIN), &win_w, &win_h);

    GdkPixbuf *buf = gdk_pixbuf_new_from_file(fn, &err);

    if (buf == NULL) {
        fprintf(stderr, "Could not open %s - %s\n", fn, err->message);
        g_error_free(err);
        return;
    }


    ORIG_W = gdk_pixbuf_get_width(buf);
    ORIG_H = gdk_pixbuf_get_height(buf);

    g_object_unref((GObject *) ORIG_BUF);
    ORIG_BUF = buf;

    if (win_w >= ORIG_W && win_h >= ORIG_H) {
        gtk_image_set_from_pixbuf((GtkImage *) IMG, ORIG_BUF);
        return;
    }

    int new_w;
    int new_h;
    GdkPixbuf *newbuf = NULL;

    get_new_w_h((double) ORIG_W, (double) ORIG_H,
                (double) win_w, (double) win_h,
                &new_w, &new_h);
    
    if (new_w < MIN_IMG_SIZE || new_h < MIN_IMG_SIZE) {
        int m = new_w > new_h ? new_w : new_h;
        int diff = MIN_IMG_SIZE - m;
        new_w += diff;
        new_h += diff;
    }

    newbuf = gdk_pixbuf_scale_simple(ORIG_BUF, new_w, new_h,
                                     GDK_INTERP_BILINEAR);
    
    gtk_image_set_from_pixbuf((GtkImage *) IMG, newbuf);

    g_object_unref((GObject *) newbuf);
}

gboolean read_new_img_callback(GIOChannel *chan,
                               GIOCondition cond,
                               gpointer notused)
{
    if ((cond & G_IO_IN) != G_IO_IN) {
        g_io_channel_shutdown(chan, FALSE, NULL);
        return FALSE;
    }

    int BUF_SIZE = 256;
    char buf[BUF_SIZE];
    gsize bytes_read;
    GError *read_err = NULL;
    int i;

    GIOStatus status = g_io_channel_read_chars(chan,
                                               buf,
                                               BUF_SIZE,
                                               &bytes_read,
                                               &read_err);

    char title[(int) bytes_read + 7];
    buf[bytes_read] = '\0';
    //printf("read (%d bytes) \"%s\" \n",(int) bytes_read,
    //       bytes_read > 0 ? buf: "");


    switch (status) {

    case G_IO_STATUS_NORMAL:

        i = bytes_read - 1;
        while (isspace(buf[i]))
            i--;

        buf[i+1] = '\0';
        

        set_scaled_image_from_file(buf);

        snprintf(title, bytes_read + 7, "pimg - %s", buf);

        gtk_window_set_title(GTK_WINDOW(WIN), title);

        // fprintf(stderr, "set new image \"%s\"\n", buf);
        
        return TRUE;

    case G_IO_STATUS_ERROR:
        fprintf(stderr, "[ERROR] while reading image file %s\n",
                read_err->message);

        g_error_free(read_err);
        break;

    case G_IO_STATUS_EOF:
        fprintf(stderr, "EOF recieved... quitting\n");
        break;

    case G_IO_STATUS_AGAIN:
        fprintf(stderr, "Got to STATUS_AGAIN\n");
        break;

    default:
        fprintf(stderr, "got to default\n");
    }

    g_io_channel_shutdown(chan, FALSE, NULL);
    exit(1);
    

    // not reached
    fprintf(stderr, "What's going on here, reach the UNCREAHABLE!");
    return FALSE;
}

void my_log_handler(const gchar *log_domain,
                    GLogLevelFlags log_level,
                    const gchar *msg,
                    gpointer user_data)
{
#ifdef DEBUG
    fprintf(stderr, "(%s) %s\n", log_domain, msg);
#endif
    return;
}


int main(int argc, char *argv[])
{
    GLogLevelFlags all = G_LOG_LEVEL_MASK;
        
    gtk_init(&argc, &argv);

    g_log_set_handler("Gtk", all, my_log_handler, NULL);
    g_log_set_handler("Glib",all, my_log_handler, NULL);
    g_log_set_handler("pimg", all, my_log_handler, NULL);
    g_log_set_default_handler(my_log_handler, NULL);

    WIN = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    g_signal_connect(WIN, "delete_event", gtk_main_quit, NULL);
    gtk_window_set_title(GTK_WINDOW(WIN), "pimg");
    gtk_window_set_default_size(GTK_WINDOW(WIN), 400, 300);


    IMG = gtk_image_new();
    ORIG_BUF = NULL;
    ORIG_W = 0;
    ORIG_H = 0;
    g_signal_connect(WIN, "size-allocate", G_CALLBACK(resize_image), NULL);

    GIOChannel *chan = g_io_channel_unix_new(STDIN_FILENO);
    g_io_channel_set_flags(chan, G_IO_FLAG_NONBLOCK, NULL);
    g_io_add_watch(chan,
                   (G_IO_IN | G_IO_HUP | G_IO_ERR | G_IO_NVAL),
                   read_new_img_callback,
                   NULL);
    g_io_channel_unref(chan);

    gtk_container_add(GTK_CONTAINER (WIN), IMG);
    gtk_widget_show_all((GtkWidget *) WIN);
    gtk_main();

    return 0;
}
